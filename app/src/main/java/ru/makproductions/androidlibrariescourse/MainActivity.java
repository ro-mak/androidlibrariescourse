package ru.makproductions.androidlibrariescourse;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import ru.makproductions.androidlibrariescourse.presenter.MainPresenter;
import ru.makproductions.androidlibrariescourse.view.MainView;

public class MainActivity extends MvpAppCompatActivity implements View.OnClickListener, MainView {

    Button buttonOne;
    Button buttonTwo;
    Button buttonThree;

    @InjectPresenter
    MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonOne = findViewById(R.id.btnCounter1);
        buttonTwo = findViewById(R.id.btnCounter2);
        buttonThree = findViewById(R.id.btnCounter3);

        buttonOne.setOnClickListener(this);
        buttonTwo.setOnClickListener(this);
        buttonThree.setOnClickListener(this);
    }

    @ProvidePresenter
    public MainPresenter provideMainPresenter() {
        return new MainPresenter();
    }

    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btnCounter1:
                presenter.counterClick(0);
                break;
            case R.id.btnCounter2:
                presenter.counterClick(1);
                break;
            case R.id.btnCounter3:
                presenter.counterClick(2);
                break;
        }
    }

    @Override
    public void setButtonOneValue(int value) {
        buttonOne.setText(String.format(getString(R.string.countEquals), value));
    }

    @Override
    public void setButtonTwoValue(int value) {
        buttonTwo.setText(String.format(getString(R.string.countEquals), value));
    }

    @Override
    public void setButtonThreeValue(int value) {
        buttonThree.setText(String.format(getString(R.string.countEquals), value));
    }
}
