package ru.makproductions.androidlibrariescourse.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import ru.makproductions.androidlibrariescourse.model.Model;
import ru.makproductions.androidlibrariescourse.view.MainView;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {

    Model model;

    public MainPresenter() {
        this.model = new Model();
    }

    public int calculateButtonValue(int index) {
        model.setAt(index, model.getAt(index) + 1);
        return model.getAt(index);
    }

    public void counterClick(int index) {
        switch (index) {
            case 0:
                getViewState().setButtonOneValue(calculateButtonValue(index));
                break;
            case 1:
                getViewState().setButtonTwoValue(calculateButtonValue(index));
                break;
            case 2:
                getViewState().setButtonThreeValue(calculateButtonValue(index));
                break;
        }
    }
}
